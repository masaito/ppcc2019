import setuptools

setuptools.setup(
    name="ppcc2019",
    version='0.0.2',
    description='Material for a lecture: Deep Learning',
    author='Masahiko Saito',
    author_email='saito@icepp.s.u-tokyo.ac.jp',
    packages=setuptools.find_packages(),
    install_requires=[
        'matplotlib',
        'pandas',
        'sklearn',
        'jupyter',
        'graphviz',
        'pydotplus',
        'tensorflow',
        'keras',
    ],
)
